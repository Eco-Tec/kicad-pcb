﻿# Librarys and footprints for kicad

- [KLC Guide](http://kicad-pcb.org/libraries/klc/)
- [Library Expert Footprint Naming Convention](https://www.cskl.de/fileadmin/csk/dokumente/produkte/pcbl/ipc_standard_pcb_library_expert_Land_Pattern_Naming_Convention.pdf)

### Directory tree
- [library](/library) 
- [modules](/modules) 
- [modules/packages3d](/modules/packages3) 


## Images


## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions

See commit details to find the authors of each Part.
- @fandres323
- 

